env=DEV
app=user-api
port=8090

clean:
	./gradlew clean

build:
	./gradlew build

run:
	./gradlew bootRun -Pport=${port} -Penv=${env}


swagger:
	./gradlew generateSwagger