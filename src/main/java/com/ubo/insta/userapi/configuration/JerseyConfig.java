package com.ubo.insta.userapi.configuration;

import com.ubo.insta.userapi.check.CheckController;
import com.ubo.insta.userapi.rights.RightsController;
import com.ubo.insta.userapi.shared.filters.AuthenticationRequiredImpl;
import com.ubo.insta.userapi.shared.handlers.UserExceptionHanlder;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
@ApplicationPath("/api/v1")
public class JerseyConfig  extends ResourceConfig {

  public JerseyConfig() {
    register(CheckController.class);
    register(AuthenticationRequiredImpl.class);
    register(UserExceptionHanlder.class);
    register(RightsController.class);
  }
}
