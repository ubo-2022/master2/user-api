package com.ubo.insta.userapi.rights;

import com.ubo.insta.userapi.rights.entity.RightEntity;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.stereotype.Component;

@Component
public class RightsBusiness {

 @Inject
 private RightsRepository rightsRepository;

 public List<RightEntity> getRights(String userId) {
  return rightsRepository.getUserRights(userId)
    .stream()
    .map(RightsMapper::map)
    .collect(Collectors.toList());
 }
}
