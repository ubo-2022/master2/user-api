package com.ubo.insta.userapi.rights;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.stereotype.Controller;

@Controller
@Path("/users")
public class RightsController {

  @Inject
  private RightsBusiness rightsBusiness;

  @GET
  @Path("/{userId}/rights")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response getRights(@PathParam("userId") String userId) {
    var rights = rightsBusiness.getRights(userId)
      .stream()
      .map(RightsMapper::map).toList();
    return Response.ok(rights).build();
  }

}
