package com.ubo.insta.userapi.rights;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import swagger.userapi.Right;

@Component
public class RightsRepository {

 private static final String SQL_GET_USER_RIGHTS =
   "SELECT USER_ID, RIGHT_ID, LABEL, ISACTIVE FROM RIGHTS WHERE USER_ID = :userID";

 @Inject
 private NamedParameterJdbcTemplate instaTemplate;

 public List<Right> getUserRights(String userId) {
  Map<String, String> params = new HashMap<>();
  params.put("userID", userId);
  return instaTemplate.query(SQL_GET_USER_RIGHTS, params, (row, number) -> {
   Right right = new Right();
   right.setRightName(row.getString("LABEL"));
   right.setId(row.getString("RIGHT_ID"));
   var isActive = Integer.valueOf(row.getString("ISACTIVE"));
   right.setIsActive(isActive == 1);
   return right;
  });
 }
}
