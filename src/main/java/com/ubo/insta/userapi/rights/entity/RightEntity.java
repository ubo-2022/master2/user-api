package com.ubo.insta.userapi.rights.entity;

public record RightEntity(String id, String label, boolean isAvtive) {

}
