package com.ubo.insta.userapi.shared.filters;

import java.io.IOException;
import java.util.List;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;
import swagger.storyapi.Error;

@AuthenticationRequired
@Provider
public class AuthenticationRequiredImpl implements ContainerRequestFilter {

 @Override
 public void filter(ContainerRequestContext requestContext) throws IOException {
  var isClientHeader = requestContext.getHeaderString("IS_CLIENT");
  if (null == isClientHeader || "false".equals(isClientHeader)) {
   var error = new Error();
   error.setContent(List.of("L'utilisateur n'est pas un client"));
   error.setCode("11");
   requestContext.abortWith(Response.status(Status.UNAUTHORIZED).entity(error).build());
  }
 }
}
