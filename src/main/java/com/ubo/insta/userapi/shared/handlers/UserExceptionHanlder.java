package com.ubo.insta.userapi.shared.handlers;

import com.ubo.insta.userapi.shared.exceptions.UserException;
import java.util.List;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import swagger.userapi.Error;

public class UserExceptionHanlder implements ExceptionMapper<UserException> {

 @Override
 public Response toResponse(UserException exception) {
  var error = new Error();
  error.setCode(String.valueOf(exception.getCode()));
  error.setContent(List.of(exception.getMessage()));
  return Response.status(Status.BAD_REQUEST).entity(error).build();
 }
}
