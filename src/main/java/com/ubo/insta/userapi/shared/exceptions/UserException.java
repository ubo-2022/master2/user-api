package com.ubo.insta.userapi.shared.exceptions;

public class UserException extends RuntimeException {
 int code;
 public UserException(int code, String message) {
  super(message);
  this.code = code;
 }

 public int getCode() {
  return this.code;
 }
}
